import pandas as pd
from transformers import pipeline


def oracle(context, question, model_name="deepset/roberta-base-squad2"):
    cont = context.replace("\"", "'")
    qna = pipeline(task="question-answering", model=model_name)
    input = {
        'question': question,
        'context': cont
    }
    ret = qna(**input)
    
    #print(cont)
    #print(ret)

    return ret['answer']


if __name__ == "__main__":
    df = pd.read_csv("report.csv", encoding = "ISO-8859-1").fillna('N/A')
    tmp = df[(df['Financial Detriment'] != 'N/A') & (df['Financial Detriment'].str.len() > 10)]

    #for t in tmp['Financial Detriment'].unique():
    #if len(t) > 10:
    #        print(t)
    print(tmp.shape[0])
    model = "deepset/roberta-base-squad2"
    question = "How much money have been spent?"
    tmp['ans'] = tmp['Financial Detriment'].apply(oracle, args=(question, model))
    tmp[['ans', 'Financial Detriment']].to_csv("out.csv")
    
